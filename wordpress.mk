.PHONY: wp-nuke wp-install wp-update

wp-nuke:
	@make wp "db reset --yes"
	@rm -fr ./web
	@touch ./web/.gitkeep

wp-install:
	@make composer install
	@make wp "core download --path=web"
	@make bash "ln -s ../config/wp/wp-config.php ./web/wp-config.php"
	@make bash "rm -fr ../../web/wp-content/uploads"
	@make bash "ln -s ../../uploads ./web/wp-content/uploads"
	@make wp "core install --url=${PROJECT_BASE_URL} --title=${PROJECT_NAME} --admin_user=admin --admin_password=admin --admin_email=contact@codr.fr"
	@make wp "core language install --activate fr_FR"

wp-update:
	@make composer install