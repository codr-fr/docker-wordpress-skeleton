# Docker-based WordPress Stack

Based on [wodby/docker4wordpress](https://github.com/wodby/docker4wordpress)

## Prerequiresites

* [docker](https://docs.docker.com/desktop/)
* [traefik](https://gitlab.com/florenttorregrosa-docker/apps/docker-traefik)

## Other tools used

Theses tools are in the container

* [wp-cli](https://wp-cli.org/)
* [composer](https://getcomposer.org/)

## Installation

### Run containers

```shell
make init-dev
```

Edit .env file as needed and then:

```shell
make up
```

### First setup (fresh DB)

```shell
make wp-install
```

Site should be available @ (https://wp.docker.localhost/)

## Update plugins/themes

```shell
make composer "update -W"
```

## Custom plugins/themes

To create a custom theme/plugin (which will be versionned), create a new folder in  `app/plugins` (or `app/themes`)

Follow the classic [Wordpress plugin developpement](https://developer.wordpress.org/plugins/intro/) here.

You must add a `composer.json` file which should look like this: 

```json
{
    "name": "acme/plugin-name",
    "description": "My simple plugin",
    "type": "wordpress-plugin",
    "version": "1.0.0"
}
```

| Variable    | Description                                               |
|-------------|-----------------------------------------------------------|
| name        | Name of your plugin, will be require with that name later |
| description | A simple description of your plugin                       |
| type        | "wordpress-plugin" or "wordpress-theme"                   |
| version     | A [Semantic Versioning](https://semver.org/) value        |

Once this is done, you can require it in your WP

```shell
make composer "require acme/plugin-name"
```